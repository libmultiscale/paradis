/**************************************************************************
 *
 *      Module:  Displacement -- This module contains various functions for
 *               calculating displacements.
 *
 *      Includes functions:
 *          SolidAngleSegmentBarnett()
 *          SegmentDisplacementsBarnett()
 *
 *************************************************************************/
#ifdef PARADIS_DISPLACEMENTS

#include <stdio.h>
#include <math.h>

#ifdef PARALLEL
#include "mpi.h"
#endif

#include "Home.h"


/*---------------------------------------------------------------------------
 *
 *      Function:       SolidAngleSegmentBarnett
 *      Description:    Calculate the solid angle using the formulation in
 *                      Barnett 1985/2007
 *
 *      Input:
 *          lA, lB, lC  Unit vectors from field point to corners of triangle.
 *          N           Unit normal to slip plane.
 *
 *-------------------------------------------------------------------------*/
real8 SolidAngleSegmentBarnett(real8 *lA, real8 *lB, real8 *lC, real8 *N){
    real8 a, b, c, s, E, sign;

    a = acos( lB[0]*lC[0]+lB[1]*lC[1]+lB[2]*lC[2] );
    b = acos( lA[0]*lC[0]+lA[1]*lC[1]+lA[2]*lC[2] );
    c = acos( lA[0]*lB[0]+lA[1]*lB[1]+lA[2]*lB[2] );
    s = 0.5*(a+b+c);
    E = 4.0 * atan(sqrt( tan(0.5*s) * tan(0.5*(s-a)) * tan(0.5*(s-b)) * tan(0.5*(s-c)) ));
    // E might return NaN if the point is close to the slip plane.
    /* if(E!=E) E=2.0*M_PI; */

    if(E > 2.0*M_PI || E < 0.0 || E!=E){
        /* printf("WARNING: Illegal E (%f) in solid angle for A(%f,%f,%f), B(%f,%f,%f) and C(%f,%f,%f). It is replaced by 0. [%s line %d\n]", */
	/*        E,lA[0],lA[1],lA[2],lB[0],lB[1],lB[2],lC[0],lC[1],lC[2],__FILE__,__LINE__); */
        return 0.0;
    }
    sign = (lA[0]*N[0]+lA[1]*N[1]+lA[2]*N[2] < 0 ) ? -1.0 : 1.0;
    return -sign*E;
}

/*---------------------------------------------------------------------------
 *
 *      Function:       ComputeProjectedPoint
 *      Description:    Calculate the displacement solution for all field
 *                      points from the segment AB.
 *      Contact:        jaehyun.cho@epfl.ch
 *
 *-------------------------------------------------------------------------*/
void ComputeProjectedPoint(real8 *A,real8 *C,real8 *burg, real8 *P){
  // Marc Fivel & Christophe Depres, Philosophical Magazine, Volume 94, Issue 28, 2014
  real8 x1 = A[0] + burg[0]*1e+8;
  real8 y1 = A[1] + burg[1]*1e+8;
  real8 z1 = A[2] + burg[2]*1e+8;

  real8 x2 = A[0] - burg[0]*1e+8;
  real8 y2 = A[1] - burg[1]*1e+8;
  real8 z2 = A[2] - burg[2]*1e+8;
  
  real8 x0 = C[0];
  real8 y0 = C[1];
  real8 z0 = C[2];

  real8 ap1 = x0-x1;
  real8 ap2 = y0-y1;
  real8 ap3 = z0-z1;
  
  real8 ab1 = x2-x1;
  real8 ab2 = y2-y1;
  real8 ab3 = z2-z1;
    
  real8 dotAPAB = ap1*ab1 + ap2*ab2 + ap3*ab3;
  real8 dotABAB = ab1*ab1 + ab2*ab2 + ab3*ab3;
  
  

  
  /* real8 nom = (x1-x0)*(x2-x1) + (y1-y0)*(y2-y1) + (z1-z0)*(z2-z1); */
  /* real8 dnom= (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1); */

  /* real8 t = -1.0*nom/dnom; */
  
  P[0] = x1 + dotAPAB/dotABAB*ab1;
  P[1] = y1 + dotAPAB/dotABAB*ab2;
  P[2] = z1 + dotAPAB/dotABAB*ab3;

}

void SegmentDisplacementsOfAtom(real8 *A, real8 *B, real8 *C, real8 *burg, 
				real8 *dispPointU, real8 *dispPointX, 
				real8 pois);

/*---------------------------------------------------------------------------
 *
 *      Function:       SegmentDisplacementsBarnett
 *      Description:    Calculate the displacement solution for all field
 *                      points from the segment AB. Solution from Barnett 1985/2007.
 *
 *-------------------------------------------------------------------------*/
void SegmentDisplacementsBarnett(Home_t *home, real8 *A, real8 *B, real8 *C, real8 *burg){
  int c,i;
  real8 pois = home->param->pois;
  for(i=0; i<home->dispPointCount; ++i){
    c = 3*i;
    /* real8 disp[3] = {0.0, 0.0, 0.0}; */
    /* real8 point[3]= {home->dispPointX[c],home->dispPointX[c+1],home->dispPointX[c+2]}; */
    /* SegmentDisplacementsOfAtom(A,B,C,burg,disp,point,pois); */
    /* home->dispPointU[c]  = disp[0]; */
    /* home->dispPointU[c+1]= disp[1]; */
    /* home->dispPointU[c+2]= disp[2]; */

    real8 disp1[3] = {0.0, 0.0, 0.0};
    real8 disp2[3] = {0.0, 0.0, 0.0};

    real8 point[3]= {home->dispPointX[c],home->dispPointX[c+1],home->dispPointX[c+2]};

    real8 P1[3] = {0.0, 0.0, 0.0};
    real8 P2[3] = {0.0, 0.0, 0.0};
    ComputeProjectedPoint(A,C,burg,P1);
    ComputeProjectedPoint(B,C,burg,P2);

    SegmentDisplacementsOfAtom(P2,A,B,burg,disp1,point,pois);
    home->dispPointU[c]  += disp1[0];
    home->dispPointU[c+1]+= disp1[1];
    home->dispPointU[c+2]+= disp1[2];

    SegmentDisplacementsOfAtom(P1,A,P2,burg,disp2,point,pois);
    home->dispPointU[c]  += disp2[0];
    home->dispPointU[c+1]+= disp2[1];
    home->dispPointU[c+2]+= disp2[2];

    /* home->dispPointU[c]  += disp[0]; */
    /* home->dispPointU[c+1]+= disp[1]; */
    /* home->dispPointU[c+2]+= disp[2]; */
  }
}


/*---------------------------------------------------------------------------
 *
 *      Function:       SegmentDisplacementsOfAtom
 *      Description:    Calculate the displacement solution for all field
 *                      points from the segment AB. Solution from Barnett 1985/2007.
 *
 *-------------------------------------------------------------------------*/
void SegmentDisplacementsOfAtom(real8 *A, real8 *B, real8 *C, real8 *burg, 
				real8 *dispPointU, real8 *dispPointX, 
				real8 pois){
  int i,j,c, calcSA=1;
  real8 lA[3], lB[3], lC[3], N[3];
  real8 tAB[3], tBC[3], tCA[3];
  real8 fAB[3], fBC[3], fCA[3];
  real8 gAB[3], gBC[3], gCA[3];
  real8 ABnorm, BCnorm, CAnorm, RAnorm, RBnorm, RCnorm;
  real8 ThetaABC, c1_inv, c2, c3, c4;
  
  real8 AB[3] = { B[0]-A[0], B[1]-A[1], B[2]-A[2] };
  real8 BC[3] = { C[0]-B[0], C[1]-B[1], C[2]-B[2] };
  real8 CA[3] = { C[0]-A[0], C[1]-A[1], C[2]-A[2] };
  
  
  ABnorm = Normal(AB);
  BCnorm = Normal(BC);
  CAnorm = Normal(CA);
  
  // Zero length segment; nothing to do.
  if(ABnorm<1.0e-20) return;
  if(BCnorm<1.0e-20 || CAnorm<1.0e-20){
    //printf("WARNING: Illegal choice of C, at %s line %d\n",__FILE__,__LINE__);
    return;
  }

  // There will be no contribution to solid angle if AB and BC are colinear.
  cross(AB,BC,N);
  c2 = Normal(N);
  if(c2 < 1.0e-20){
    calcSA = 0;
    ThetaABC = 0.0;
  }else{
    N[0] /= c2;
    N[1] /= c2;
    N[2] /= c2;
  }
  
  tAB[0] = AB[0]/ABnorm; tAB[1] = AB[1]/ABnorm; tAB[2] = AB[2]/ABnorm;
  tBC[0] = BC[0]/BCnorm; tBC[1] = BC[1]/BCnorm; tBC[2] = BC[2]/BCnorm;
  tCA[0] = CA[0]/CAnorm; tCA[1] = CA[1]/CAnorm; tCA[2] = CA[2]/CAnorm;
  
  // Iterate over defined field points.
  c1_inv = 1.0 / (8.0*M_PI*(1-pois));
  
  //for(i=0; i<home->dispPointCount; ++i){
  // Calculate lambda vectors (unit vectors from field point to triangle).
  //  c = 3*i;
  for(j=0; j<3; ++j){
    lA[j] = A[j] - dispPointX[j];
    lB[j] = B[j] - dispPointX[j];
    lC[j] = C[j] - dispPointX[j];
  }
  RAnorm = Normal(lA);
  RBnorm = Normal(lB);
  RCnorm = Normal(lC);
  for(j=0; j<3; ++j){
    lA[j] /= RAnorm;
    lB[j] /= RBnorm;
    lC[j] /= RCnorm;
  }
  
  // Calculate the solid angle. There should be no contribution
  // to solid angle if the point lies on the slip plane.
  if(calcSA) ThetaABC = SolidAngleSegmentBarnett(lA,lB,lC,N);
  
  // Calculate Barnett fAB = (b x tAB) ln[(Rb/Ra) . ((1 + lB . tAB)/(1 + lA . tAB))]
  cross(burg,tAB,fAB);
  cross(burg,tBC,fBC);
  cross(burg,tCA,fCA);
  c2 = (1 + lB[0]*tAB[0]+lB[1]*tAB[1]+lB[2]*tAB[2])  /  (1 + lA[0]*tAB[0]+lA[1]*tAB[1]+lA[2]*tAB[2]);
  c3 = (1 + lC[0]*tBC[0]+lC[1]*tBC[1]+lC[2]*tBC[2])  /  (1 + lB[0]*tBC[0]+lB[1]*tBC[1]+lB[2]*tBC[2]);
  c4 = (1 + lA[0]*tCA[0]+lA[1]*tCA[1]+lA[2]*tCA[2])  /  (1 + lC[0]*tCA[0]+lC[1]*tCA[1]+lC[2]*tCA[2]);
  c2 = log(RBnorm/RAnorm * c2);
  c3 = log(RCnorm/RBnorm * c3);
  c4 = log(RAnorm/RCnorm * c4);
  fAB[0] *= c2; fAB[1] *= c2; fAB[2] *= c2;
  fBC[0] *= c3; fBC[1] *= c3; fBC[2] *= c3;
  fCA[0] *= c4; fCA[1] *= c4; fCA[2] *= c4;
  
  // Calculate Barnett gAB = [b . (lA x lB)](lA + lB) / (1 + lA . lB)
  cross(lA,lB,gAB);
  cross(lB,lC,gBC);
  cross(lC,lA,gCA);
  c2 = (burg[0]*gAB[0]+burg[1]*gAB[1]+burg[2]*gAB[2]) / (1 + lA[0]*lB[0]+lA[1]*lB[1]+lA[2]*lB[2]);
  c3 = (burg[0]*gBC[0]+burg[1]*gBC[1]+burg[2]*gBC[2]) / (1 + lB[0]*lC[0]+lB[1]*lC[1]+lB[2]*lC[2]);
  c4 = (burg[0]*gCA[0]+burg[1]*gCA[1]+burg[2]*gCA[2]) / (1 + lC[0]*lA[0]+lC[1]*lA[1]+lC[2]*lA[2]);
  for(j=0; j<3; ++j){
    gAB[j] = (lA[j]+lB[j]) * c2;
    gBC[j] = (lB[j]+lC[j]) * c3;
    gCA[j] = (lC[j]+lA[j]) * c4;
  }
  
  // Update the displacements.
  c2 = ThetaABC/(4.0*M_PI);
  c3 = (1.0 - 2.0*pois)*c1_inv;
  for(j=0; j<3; ++j){
    dispPointU[j] = c1_inv*(gAB[j]+gBC[j]+gCA[j]) - c2*burg[j] - c3*(fAB[j]+fBC[j]+fCA[j]);
  }
}

#endif

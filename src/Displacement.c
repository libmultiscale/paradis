/**************************************************************************
 *
 *      Module:  Displacement -- This module contains various functions for
 *               calculating displacements.
 *
 *      Includes functions:
 *          ZeroPointDisplacements()
 *          ReadDisplacementPoints()
 *          CreateDisplacementGrid()
 *          GetDisplacementPoints()
 *          PointDisplacements()
 *
 *************************************************************************/
#ifdef PARADIS_DISPLACEMENTS

#include <stdio.h>
#include <math.h>

#ifdef PARALLEL
#include "mpi.h"
#endif

#include "Home.h"


/*---------------------------------------------------------------------------
 *
 *      Function:       ZeroPointDisplacements
 *      Description:    Reset the displacements at all points to zero.
 *
 *-------------------------------------------------------------------------*/
void ZeroPointDisplacements(Home_t *home){
    int i,j,c;
    for(i=0; i<home->dispPointCount; ++i){
        c = 3*i;
        for(j=0; j<3; ++j)
            home->dispPointU[c+j] = 0.0;
    }
}


/*---------------------------------------------------------------------------
 *
 *      Function:       ReadDisplacementPoints
 *      Description:    Read the input file containing all the points where
 *                      displacements should be calculated.
 *
 *-------------------------------------------------------------------------*/
void ReadDisplacementPoints(Home_t *home){
    int i, c;
    char inLine[500];
    FILE *file;

    memset(inLine, 0, sizeof(inLine));
    file = (FILE *)NULL;

    // Only the root reads the file.
    if(home->myDomain == 0){
        // Try to open the file.
        if(home->param->dispPointsInput==(char *)NULL)
            Fatal("ReadDisplacementPoints: No data file provided.");
        if( (file=fopen(home->param->dispPointsInput,"r")) == (FILE *)NULL )
            Fatal("Error %d opening file %s to read displacement points.",errno,home->param->dispPointsInput);

        // Read the first line, which should be the number of points.
        Getline(inLine, sizeof(inLine), file);
        sscanf(inLine, "%d", &home->dispPointCount);

        // Allocate memory.
        if(home->dispPointX != NULL){ free(home->dispPointX); home->dispPointX=NULL; }
        if(home->dispPointU != NULL){ free(home->dispPointU); home->dispPointU=NULL; }
        home->dispPointX = (real8 *)malloc(3*home->dispPointCount*sizeof(real8));
        home->dispPointU = (real8 *)malloc(3*home->dispPointCount*sizeof(real8));

        // Read in points.
        for(i=0; i<home->dispPointCount; ++i){
            Getline(inLine, sizeof(inLine), file);
            // If we hit EOF, print a warning and break the loop.
            if(inLine[0] == 0){
                printf("WARNING: Mismatched number of displacement points, at %s line %d\n",
                        __FILE__, __LINE__);
                home->dispPointCount = i;
                break;
            }
            // Process the line.
            c = 3*i;
            sscanf(inLine, "%lf %lf %lf",&home->dispPointX[c  ],
                                         &home->dispPointX[c+1],
                                         &home->dispPointX[c+2]);
        }

        // Close the file.
        fclose(file);
        file = (FILE *)NULL;
    }

    // Distribute data to other processors.
#if PARALLEL
    // Allocate memory on other processors.
    MPI_Bcast(&home->dispPointCount,1,MPI_INT,0,MPI_COMM_WORLD);
    c = 3*home->dispPointCount;
    if(home->myDomain != 0){
        if(home->dispPointX != NULL){ free(home->dispPointX); home->dispPointX=NULL; }
        if(home->dispPointU != NULL){ free(home->dispPointU); home->dispPointU=NULL; }
        home->dispPointX = (real8 *)malloc(c*sizeof(real8));
        home->dispPointU = (real8 *)malloc(c*sizeof(real8));
    }

    // Distribute data to other processors.
    MPI_Bcast(home->dispPointX,c,MPI_DOUBLE,0,MPI_COMM_WORLD);
#endif
}


/*---------------------------------------------------------------------------
 *
 *      Function:       CreateDisplacementGrid
 *      Description:    Create a grid of field points at which we'll
 *                      calculate displacements.
 *
 *-------------------------------------------------------------------------*/
void CreateDisplacementGrid(Home_t *home){
    Param_t *param = home->param;
    int p, i, j, k, c;
    int nx=param->dispPointsGrid[0], ny=param->dispPointsGrid[1], nz=param->dispPointsGrid[2];
    real8 xmin = param->minSideX, xmax = param->maxSideX;
    real8 ymin = param->minSideY, ymax = param->maxSideY;
    real8 zmin = param->minSideZ, zmax = param->maxSideZ;
    real8 Lx = xmax-xmin, Ly = ymax-ymin, Lz = zmax-zmin;
    real8 dx = Lx/(real8)nx, dy = Ly/(real8)ny, dz = Lz/(real8)nz; 

    if(home->dispPointX != NULL){ free(home->dispPointX); home->dispPointX=NULL; }
    if(home->dispPointU != NULL){ free(home->dispPointU); home->dispPointU=NULL; }
    home->dispPointCount = nx*ny*nz;
    home->dispPointX = (real8 *)malloc(3*home->dispPointCount*sizeof(real8));
    home->dispPointU = (real8 *)malloc(3*home->dispPointCount*sizeof(real8));
    p = 0;
    for(i=0; i<nx; ++i){
        for(j=0; j<ny; ++j){
            for(k=0; k<nz; ++k){
                c = 3*p;
                home->dispPointX[c  ] = xmin + dx*i;
                home->dispPointX[c+1] = ymin + dy*j;
                home->dispPointX[c+2] = zmin + dz*k;
                ++p;
            }
        }
    }
}


/*---------------------------------------------------------------------------
 *
 *      Function:       GetDisplacementPoints
 *      Description:    Read the input file containing all the points where
 *                      displacements should be calculated.
 *
 *-------------------------------------------------------------------------*/
void GetDisplacementPoints(Home_t *home){
    if(home->param->dispPointsGrid[0]==0 ||
       home->param->dispPointsGrid[1]==0 ||
       home->param->dispPointsGrid[2]==0)
        ReadDisplacementPoints(home);
    else CreateDisplacementGrid(home);
    ZeroPointDisplacements(home);
}

/*---------------------------------------------------------------------------
 *
 *      Function:       determinant
 *      Description:    Calculate the determinant 3x3
 *      Contact:        jaehyun.cho@epfl.ch
 *
 *-------------------------------------------------------------------------*/
real8 determinant(real8 a, real8 b, real8 c, real8 d, real8 e, real8 f, real8 g, real8 h, real8 i){
  real8 det = a*(e*i - f*h) - b*(d*i - f*g) + c*(d*h - e*g);
  return det;
}


/*---------------------------------------------------------------------------
 *
 *      Function:       ComputeClosurePoint
 *      Description:    Calculate the closure point for given segment
 *      Contact:        jaehyun.cho@epfl.ch
 *
 *-------------------------------------------------------------------------*/
void ComputeClosurePoint(real8 *A, real8 *B, real8 *b, real8 *C){
  // Marc Fivel & Christophe Depres, Philosophical Magazine, Volume 94, Issue 28, 2014
  // The closure point is defined as the intersection point 
  // between arbitrary vector of u to the current slip plane.
  // The slip plane can be defined by cross product of burgers vector and vector of AB
  real8 O[] = {0.0, 0.0, 0.0};
  real8 u[] = {1.0, 1.0, 1.0}; // arbitrary vector from the origin point O
  
  // http://mathworld.wolfram.com/Line-PlaneIntersection.html
  /* if(fabs(A[1]-B[1])>1.0){ */
  /*   real8 b0 = b[0]; */
  /*   b[0] = b[2]; */
  /*   b[1] = 0.0; */
  /*   b[2] = b0; */
  /* } */

  real8 x1 = A[0]+b[0];  real8 y1 = A[1]+b[1];  real8 z1 = A[2]+b[2];
  real8 x2 = A[0];       real8 y2 = A[1];       real8 z2 = A[2];
  real8 x3 = B[0];       real8 y3 = B[1];       real8 z3 = B[2];

  real8 x4 = O[0];  real8 y4 = O[1];  real8 z4 = O[2];
  real8 x5 = u[0];  real8 y5 = u[1];  real8 z5 = u[2];
  
  real8 nom11 = determinant(x2,x3,x4,y2,y3,y4,z2,z3,z4);
  real8 nom12 = determinant(x1,x3,x4,y1,y3,y4,z1,z3,z4);
  real8 nom13 = determinant(x1,x2,x4,y1,y2,y4,z1,z2,z4);
  real8 nom14 = determinant(x1,x2,x3,y1,y2,y3,z1,z2,z3);
  real8 nom = nom11 - nom12 + nom13 - nom14;

  real8 denom11 = determinant(x2,x3,x5-x4,y2,y3,y5-y4,z2,z3,z5-z4);
  real8 denom12 = determinant(x1,x3,x5-x4,y1,y3,y5-y4,z1,z3,z5-z4);
  real8 denom13 = determinant(x1,x2,x5-x4,y1,y2,y5-y4,z1,z2,z5-z4);
  real8 denom = denom11 - denom12 + denom13;

  real8 t = -1.0*nom/(denom+1e-8);
  C[0] = x4 + (x5 - x4)*t;
  C[1] = y4 + (y5 - y4)*t;
  C[2] = z4 + (z5 - z4)*t;

  /* if(fabs(A[1]-B[1])>1.0){ */
  /*   real8 b0 = b[0]; */
  /*   b[0] = b[2]; */
  /*   b[1] = 0.0; */
  /*   b[2] = b0; */
  /* } */
}

/*---------------------------------------------------------------------------
 *
 *      Function:       PointDisplacements
 *      Description:    Calculate the displacements at all given field
 *                      points.
 *
 *-------------------------------------------------------------------------*/
void PointDisplacements(Home_t *home){
    // TODO - how to deal with periodic boundary conditions?
    int i,j,k;
    int n1Dom, n2Dom;
    Node_t *node1, *node2;
    real8 A[3], B[3], burg[3];
    TimerStart(home, CALC_DISPLACEMENT);
    real8 burgMag = home->param->burgMag;
    real8 burgMagAngstrom = burgMag*1.0e+10;

    // Reset all displacements to zero.
    ZeroPointDisplacements(home);

    /* // Find the center of the loop - TODO assumes a SINGLE, FLAT, dislocation loop. */
    /* real8 C[3] = { 0.0, 0.0, 0.0 }; */
    /* j = 0; */
    for(i=0; i<home->newNodeKeyPtr; ++i){
        if((node1=home->nodeKeys[i]) == (Node_t *)NULL) continue;
	// TODO - temporary code to set dislocation displacement slip direction.
	for(k=0; k<node1->numNbrs; ++k) node1->slip[k] =  1;
        if(node1->myTag.domainID < home->myDomain) continue;
        /* C[0] += node1->x; */
        /* C[1] += node1->y; */
        /* C[2] += node1->z; */
        /* ++j; */
    }

/* #ifdef PARALLEL */
/* #ifdef PARADIS_IN_LIBMULTISCALE */
/*     MPI_Allreduce(MPI_IN_PLACE, &j, 1, MPI_INT, MPI_SUM, home->MPI_COMM_PARADIS); */
/*     MPI_Allreduce(MPI_IN_PLACE, C, 3, MPI_DOUBLE, MPI_SUM, home->MPI_COMM_PARADIS); */
/* #else */
/*     MPI_Allreduce(MPI_IN_PLACE, &j, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD); */
/*     MPI_Allreduce(MPI_IN_PLACE, C, 3, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD); */
/* #endif */
/* #endif */
/*     C[0] /= j; C[1] /= j; C[2] /= j; */

    // Iterate over nodes to create segments.
    for(i=0; i<home->newNodeKeyPtr; ++i){
        if((node1=home->nodeKeys[i]) == (Node_t *)NULL) continue;
        n1Dom = node1->myTag.domainID;
        if(n1Dom < home->myDomain) continue;
        A[X] = node1->x;
        A[Y] = node1->y;
        A[Z] = node1->z;

        for(j=0; j<node1->numNbrs; ++j){
            n2Dom = node1->nbrTag[j].domainID;
            // Avoid double-counting at boundaries by calculating on lower processor.
            if(n1Dom!=home->myDomain && n2Dom!=home->myDomain) continue;
            if(n2Dom < home->myDomain) continue;
            node2 = GetNeighborNode(home, node1, j);
            if(node2 == (Node_t *)NULL) {
                printf("WARNING: Neighbor not found at %s line %d\n", __FILE__, __LINE__);
                continue;
            }
            // Insure node1 is the node with the lower tag to avoid double-counting.
            if(OrderNodes(node1,node2)>=0) continue;

            B[X] = node2->x;
            B[Y] = node2->y;
            B[Z] = node2->z;

            burg[X] = node1->burgX[j]*burgMagAngstrom;
            burg[Y] = node1->burgY[j]*burgMagAngstrom;
            burg[Z] = node1->burgZ[j]*burgMagAngstrom;

	    // Compute closure point for given AB segment
	    real8 C[3] = { 0.0, 0.0, 0.0 };
	    ComputeClosurePoint(A,B,burg,C);
	    /* printf("A [%f %f %f], B[%f, %f, %f], C[%f, %f, %f]\n",A[0],A[1],A[2],B[0],B[1],B[2], C[0],C[1],C[2]); */

            // Calculate the displacements due to this segment.
            if(node1->slip[j] == 1)
	      SegmentDisplacementsBarnett(home, B, A, C, burg);
            else if(node1->slip[j] == -1)
	      SegmentDisplacementsBarnett(home, A, B, C, burg);
            else printf("WARNING: unknown slip direction on node [%f %f %f]\n",A[0],A[1],A[2]);
        }
    }

    // Distribute the displacements across all processors.
#ifdef PARALLEL
#ifndef PARADIS_IN_LIBMULTISCALE
    MPI_Allreduce(MPI_IN_PLACE, home->dispPointU, 3*home->dispPointCount,
                  MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#endif
#endif
    TimerStop(home, CALC_DISPLACEMENT);
}

#endif

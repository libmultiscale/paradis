/***************************************************************************
 *
 *  Tag.h  Define the tag struct used to uniquely identify a node as a 
 *         combination of home domain and local index in domain
 *
 ***************************************************************************/
#ifndef _Tag_h
#define _Tag_h

#include "Typedefs.h"

struct _tag {

  int domainID ;
  int index ;
  // added by jcho
  // Index number will be redefined in case of multiple domains (processors),
  // so the original index number defined in nodal data file is saved in index0.
  int index0; 
  // added by guillaume
  int node_number;
} ;

#endif
